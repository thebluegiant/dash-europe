import os
import dash
import flask

from dash.dependencies import Input, Output, State, Event
import dash_core_components as dcc
import dash_html_components as html

import pandas as pd
import numpy as np
import math
from chunk import Chunk

import random
import cProfile

MAPBOX = 'pk.eyJ1IjoiZGVlamVwIiwiYSI6ImNrNTMyM3o4MzAzbzczanBlNTBpa295ZW0ifQ.D0F5Ky3NspnjPSuY33OflQ'
FONT_COLOR = 'rgb(50, 50, 50)'
DOT_SIZE = 7;
TOKM = 0.001

LEGEND = {'nbofroads' : 'Total Streets',
          'population' : 'People', 
          'bikenetsize' : 'Bike-only Streets (%)',
          'pedestriannetsize' : 'Pedestrian-only Streets (%)',
          'walkablenetsize' : 'Walkable Streets (%)',
          'bikablenetsize' : 'Bikable Streets (%)'}

COMPCOLORS = ['rgba(0,255,0,0.33)', 'rgba(255,0,0,0.33)']         #city1, city2
DOTCOLORS = ['rgb(0,255,0)', 'rgb(255,0,0)', 'rgb(30,144,255)'] #city1, city2, regular


#Loading Data
eu = pd.read_csv('features_eu.csv').dropna(how='any', axis = 0)


countryList = sorted(eu['country'].unique())
countryOptions = [{'label': country, 'value': country} for country in countryList]
citiesOptions = [{'label': city, 'value': city} for city in eu['city']]
initCountry = ['Netherlands','France']


def transform_value(value):
 if(type(value) == list):
     valueList = []
     for i in value:
         valueList.append((10 ** i) + 1)
     return valueList
 else:
     return 10 ** value + 1


def getCityData(traces, cityName, feature, colorIndex):
    
    selected = pd.DataFrame(eu[(eu['city'].isin([cityName]))])
    data = np.array(selected.iloc[:,[selected.columns.get_loc(feature)]])[0]
    color = COMPCOLORS[colorIndex]
    
    traces.append({
                'type' : 'bar',
                'y' : data,
                'x' : [LEGEND[feature]],
                'text': data,
                'hoverinfo' : 'text',
                'marker' : {
                    'color' : color,
                    'line'  : {'width' : 0, 
                               'color' : '#444'
                            }
                        },
                 'name' : cityName
        })    

    
def getCityDataPercentage(traces, cityName, feature, RefFeature, colorIndex):
    selected = pd.DataFrame(eu[(eu['city'].isin([cityName]))])
                
    rawdata = np.array(selected.iloc[:,[selected.columns.get_loc(feature)]])[0]
    ref = np.array(selected.iloc[:,[selected.columns.get_loc(RefFeature)]])[0]
    data = 100*rawdata / ref
    
    color = COMPCOLORS[colorIndex]
    
    traces.append({
                'type' : 'bar',
                'y' : data,
                'x' : [LEGEND[feature]],
                'text': round(data,2),
                'hoverinfo' : 'text',
                'marker' : {
                    'color' : color,
                    'line'  : {'width' : 0, 
                               'color' : '#444'
                            }
                        },
                 'name' : cityName
        })


server = flask.Flask(__name__)
server.secret_key = os.environ.get('secret_key', 'laconsiderriereesttresimportantepourlesbebesetlesderrieres')
app = dash.Dash(__name__, server=server)
app.config.supress_callback_exceptions = True
app.css.append_css({'external_url': 'https://cdn.rawgit.com/plotly/dash-app-stylesheets/2d266c578d2a6e8850ebce48fdb52759b2aef506/stylesheet-oil-and-gas.css'})  # noqa: E501


mlayout = {
    'height' : 696,
    'autosize' : True,
    'hovermode' : 'closest',
    'mapbox' : {
        'accesstoken' : MAPBOX,
        'center' : {
            'lat' : 50.1,
            'lon' : 8.6,
        },
        'zoom' : 3.5,
        'style' : 'outdoors', #basic, satellite, light, dark
    },
    'showlegend' : False,
    'margin' : {'t' : 0,
                'b' : 0,
                'l' : 0,
                'r' : 0},
    'legend' : {
        'font' : {'color' : 'white'},
        'orientation' : 'h',
        'x' : 0,
        'y' : 1.01
    }
}

blayout =  {
    'height' : 500,
    'margin' : {
        'b' : 25,
        'l' : 30,
        't' : 70,
        'r' : 0
        },
    'showlegend' : False,
    'legend' : {
        'orientation' : 'h',
        'x' : 0,
        'y' : 1.01,
        'yanchor' : 'bottom',
        },
    'xaxis' : {'tickmode' : 'array'}
}

app.layout = html.Div([
    html.Div([
            html.H1(
                    'European Cities, Compared.',
                    style={
                        'width' : '48%',
                        'display' : 'inline-block',
                        'paddingBottom' : '0%',
                        'paddingLeft' : '2%', 
                        'color' : FONT_COLOR
                        }
                    ),
              html.Div([
                        html.Div([dcc.Dropdown( id='cityOne',
                                                options=citiesOptions,
                                                multi=False                                                
                                                )],
                                 style={'width' : '50%', 
                                        'display' : 'inline-block'}),
                        html.Div([dcc.Dropdown( id='cityTwo',
                                                options=citiesOptions,
                                                multi=False
                                                )],
                                 style={"width" : '50%', 'display' : 'inline-block'})
                        ], style = {'display' : 'inline-block', 
                                    'width' : '44%', 
                                    'paddingLeft' : '2%',
                                    'paddingRight' : '4%',
                                    'paddingTop' : '3%'})
    ]),
                       
    html.Div([
        html.Div([
                  html.Div([
                            html.H5( 'Countries', style={'display' : 'inline-block'}),
                            html.Div([dcc.Dropdown(id='countryNames',
                                                   options=countryOptions,
                                                   multi=True,
                                                   value=initCountry)], 
                                     style={'paddingBottom' : '1%'})
                            ]),
                  html.Div([dcc.Graph(id="map", config={'displayModeBar': False})])
                  ],
                 style={
                    'width' : '50%',
                    'display' : 'inline-block',
                    'paddingLeft' : 33,
                    'paddingRight' : 9,
                    'boxSizing' : 'border-box'
                    }
        ),
        html.Div([  # Holds the widgets
            html.H5('Filter Cities According To'),
            html.Div( 'Number of People', style={ 'paddingBottom' : 10}),
            html.Div([
                      dcc.RangeSlider(
                        id='nbOfPeople',
                        marks={(i): '{}'.format(10**i) for i in range(0,8)},
                        step=1,
                        min=0,
                        max=7,
                        value=[0,7],
                        updatemode='drag'
                    )],
                    style={'paddingLeft' : 9}
                ),
            html.Div( 'Number of Streets', style={'paddingTop' : 20, 'paddingBottom' : 10}),
            html.Div([
                      dcc.RangeSlider(
                        id='nbOfStreets',
                        marks={(i): '{}'.format(10**i) for i in range(0,7)},
                        step=1,
                        min=0,
                        max=6,
                        value=[0,6],
                        updatemode='drag'
                    )],
                    style={'paddingLeft' : 9}
                ),
            html.H5('Comparisons', style={'paddingTop' : 20}),      
            html.Div([
                      html.Div([dcc.Graph(id='bars1', config={'displayModeBar': False})], style={'width' : '25%', 'display' : 'inline-block'}),
                      html.Div([dcc.Graph(id='bars2', config={'displayModeBar': False})], style={'width' : '25%', 'display' : 'inline-block'}),
                      html.Div([dcc.Graph(id='bars3', config={'displayModeBar': False})], style={'width' : '25%', 'display' : 'inline-block'}),
                      html.Div([dcc.Graph(id='bars4', config={'displayModeBar': False})], style={'width' : '25%', 'display' : 'inline-block'})

                    ], style={'paddingTop' : 7, 'display' : 'inline-block', 'boxSizing' : 'border-box'}
                     ),
              ],
                style={"width" : '50%',
                        'float' : 'right', 
                        'display' : 'inline-block',
                        'paddingRight' : 50, 
                        'paddingLeft' : 21,
                        'boxSizing' : 'border-box',
                        'color' : FONT_COLOR
                        })
                ])
])


@app.callback(Output('cityOne', 'options'),
              [Input('countryNames', 'value'),
               Input('nbOfPeople', 'value'),
               Input('nbOfStreets', 'value')])
def setCities1_options(country, nbOfPeople, streetNumber):
    streetNumber    = transform_value(streetNumber)
    nbOfPeople      = transform_value(nbOfPeople)
    itempop   = eu['population'].apply(lambda x: int(x))
    itemroads = eu['nbofroads'].apply(lambda x: int(x))
    cur = eu[(eu['country'].isin(country)) & 
            ((nbOfPeople[0] <= itempop) & (itempop <= nbOfPeople[1]+1)) &
            ((streetNumber[0] <= itemroads) & (itemroads <= streetNumber[1]+1))]
    return [{'label': city + ' - ' + cur['country'][cur['city']==city], 'value': city} for city in cur['city']]

@app.callback(Output('cityOne', 'value'),
              [Input('cityOne', 'options')])
def setCities1_values(available_options):
    if len(available_options) >= 1 : return available_options[0]['value']
    else : return None
     
@app.callback(Output('cityTwo', 'options'),
              [Input('countryNames', 'value'),
               Input('nbOfPeople', 'value'),
               Input('nbOfStreets', 'value')])
def setCities2_options(country, nbOfPeople, streetNumber):
    streetNumber    = transform_value(streetNumber)
    nbOfPeople      = transform_value(nbOfPeople)
    itempop   = eu['population'].apply(lambda x: int(x))
    itemroads = eu['nbofroads'].apply(lambda x: int(x))
    cur = eu[(eu['country'].isin(country)) & 
            ((nbOfPeople[0] <= itempop) & (itempop <= nbOfPeople[1]+1)) &
            ((streetNumber[0] <= itemroads) & (itemroads <= streetNumber[1]+1))]
    return [{'label': city + ' - ' + cur['country'][cur['city']==city] , 'value': city} for city in cur['city']]

@app.callback(Output('cityTwo', 'value'),
              [Input('cityTwo', 'options')])
def setCities2_values(available_options):
    if len(available_options) >= 2 : return available_options[1]['value']
    elif len(available_options) == 1: return available_options[0]['value']
    else : return None


@app.callback(
    Output('map', 'figure'),
    [Input('countryNames', 'value'),
     Input('nbOfPeople', 'value'),
     Input('nbOfStreets', 'value'),
     Input('cityOne', 'value'),
     Input('cityTwo', 'value')],
    [State('map', 'relayoutData')])
def updateMap(country, nbOfPeople, streetNumber,
              city1, city2, map_layout):
    streetNumber    = transform_value(streetNumber)
    nbOfPeople      = transform_value(nbOfPeople)
    itempop   = eu['population'].apply(lambda x: int(x))
    itemroads = eu['nbofroads'].apply(lambda x: int(x))
    
    curated2 = eu[(eu['country'].isin(country)) & 
            ((nbOfPeople[0] <= itempop) & (itempop <= nbOfPeople[1]+1)) &
            ((streetNumber[0] <= itemroads) & (itemroads <= streetNumber[1]+1))]
    
    # plotting cities
    traces = []
    traces.append({
            'type' : 'scattermapbox',
            'mode' : 'markers',
            'lat' : curated2['lat'],
            'lon' : curated2['long'],
            'marker' : {
                'color' : DOTCOLORS[2],
                'size' : DOT_SIZE
                    },
            'hoverinfo' : 'text',
            'text' : curated2['city']
        })
    
    def highlightCity (data, city, color, scaleFactor=2):
        data.append({
            'type' : 'scattermapbox',
            'mode' : 'markers+text',
            'lat' : curated2['lat'][curated2['city']==city],
            'lon' : curated2['long'][curated2['city']==city],
            'marker' : {
                'color' : color,
                'size' : DOT_SIZE*scaleFactor
                },
            'hoverinfo' : 'text',
            'text' : city,
            'textposition' : 'top right',
            'textfont' : { 'color' : color}
        })

    highlightCity(traces, city1, DOTCOLORS[1])   
    highlightCity(traces, city2, DOTCOLORS[0])

    # saving current map camera pose
    if (map_layout is not None):
        lon = float(map_layout['mapbox']['center']['lon'])
        lat = float(map_layout['mapbox']['center']['lat'])
        zoom = float(map_layout['mapbox']['zoom'])
        mlayout['mapbox']['center']['lon'] = lon
        mlayout['mapbox']['center']['lat'] = lat
        mlayout['mapbox']['zoom'] = zoom
    else:
        lon = 50.1
        lat = 8.6
        zoom = 7
    
    fig = dict(data=traces, layout=mlayout)
    return fig


@app.callback(Output('bars1', 'figure'),
              [Input('cityOne', 'value'),
               Input('cityTwo', 'value')])
def updateBars1(city1, city2):
    traces = []
    try:
        getCityData(traces, city1, 'nbofroads', 1)
        getCityData(traces, city2, 'nbofroads', 0)
    except:
        pass
    
    figure = {'data' : traces, 'layout' : blayout}
    
    return figure


@app.callback(Output('bars2', 'figure'),
              [Input('cityOne', 'value'),
               Input('cityTwo', 'value')])
def updateBars2(city1, city2):
    traces = []
    
    try:
        getCityData(traces, city1, 'population', 1)
        getCityData(traces, city2, 'population', 0)
    except:
        pass
    
    figure = {'data' : traces, 'layout' : blayout}
    
    return figure

@app.callback(Output('bars3', 'figure'),
              [Input('cityOne', 'value'),
               Input('cityTwo', 'value')])
def updateBars3(city1, city2):
    traces = []
    
    try:
        getCityDataPercentage(traces, city1, 'bikenetsize', 'netsize', 1)
        getCityDataPercentage(traces, city2, 'bikenetsize', 'netsize', 0)
    except:
        pass
    
    figure = {'data' : traces, 'layout' : blayout}
    
    return figure

@app.callback(Output('bars4', 'figure'),
              [Input('cityOne', 'value'),
               Input('cityTwo', 'value')])
def updateBars4(city1, city2):
    traces = []
    
    try:
        getCityDataPercentage(traces, city1, 'pedestriannetsize', 'netsize', 1)
        getCityDataPercentage(traces, city2, 'pedestriannetsize', 'netsize', 0)
    except:
        pass
    
    figure = {'data' : traces, 'layout' : blayout}
    
    return figure

# Run the Dash app
if __name__ == '__main__':
    app.server.run(port=5002, debug=False, threaded=True)
